#!/usr/bin/env python

from setuptools import setup

# The version is updated automatically with bumpversion
# Do not update manually
__version = '2.0.0'


def main():

    setup(
        name='brukerecps',
        version=__version,
        package_dir={'brukerecps': 'brukerecps'},
        packages=['brukerecps'],
        author='Lothar Krause and Sergi Blanch',
        author_email='controls-software@cells.es',
        description='Tango device servers for controlling BrukerEC PS',
        license='GPLv3+',
        platforms=['src', 'noarch'],
        url='https://gitlab.com/tango-controls/device-servers/DeviceClasses/'
            'powersupply/brukerec_ps',
        requires=['tango (>=7.2.6)'],
        entry_points={
            'console_scripts': [
                'BrukerBend_PS = brukerecps.BrukerBend_PS:main',
                'BrukerEC_PS = brukerecps.BrukerEC_PS:main',
            ],
        },
    )


if __name__ == "__main__":
    main()
